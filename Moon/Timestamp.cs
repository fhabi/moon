namespace Moon
{
    public class Timestamp
    {
        private int Hour { get; }
        private int Minute { get; }
        
        public int Absolute { get; set; }

        public Timestamp(string value)
        {
            var match = Constants.TimestampRegex.Match(value);
            Hour = int.Parse(match.Groups["h"].Value);
            Minute = int.Parse(match.Groups["m"].Value);
            Absolute = int.Parse(match.Groups["h"].Value + match.Groups["m"].Value);
        }

        public Timestamp(Timestamp ts)
        {
            Hour = ts.Hour;
            Minute = ts.Minute;
            Absolute = ts.Absolute;
        }
    }
}