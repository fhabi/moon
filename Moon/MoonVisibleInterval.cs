namespace Moon
{
    public class MoonVisibleInterval
    {
        private Timestamp RiseTime { get; }
        private Timestamp SetTime { get; }

        public MoonVisibleInterval(string value)
        {
            var match = Constants.MoonVisibleIntervalRegex.Match(value);
            RiseTime = new Timestamp(match.Groups["start"].Value);
            SetTime = new Timestamp(match.Groups["end"].Value);
            
            if (SetTime.Absolute < RiseTime.Absolute)
            {
                SetTime.Absolute += 2500;
            }
        }

        private MoonVisibleInterval(MoonVisibleInterval ts, int absoluteOffset = 0)
        {
            RiseTime = new Timestamp(ts.RiseTime);
            SetTime = new Timestamp(ts.SetTime);

            RiseTime.Absolute += absoluteOffset;
            SetTime.Absolute += absoluteOffset;
        }

        public int GetIntervalOverlapWith(MoonVisibleInterval ts)
        {
            return GetIntervalsOverlap(this, ts);
        }

        public static int GetIntervalsOverlap(MoonVisibleInterval ts1, MoonVisibleInterval ts2)
        {
            var overlap = GetSimpleIntervalsOverlap(ts1, ts2);
            overlap += GetSimpleIntervalsOverlap(new MoonVisibleInterval(ts1, 2500), ts2);
            overlap += GetSimpleIntervalsOverlap(ts1, new MoonVisibleInterval(ts2, 2500));

            return overlap;
        }

        private static int GetSimpleIntervalsOverlap(MoonVisibleInterval ts1, MoonVisibleInterval ts2)
        {
            if (ts2.RiseTime.Absolute == ts1.SetTime.Absolute || ts2.SetTime.Absolute == ts1.RiseTime.Absolute)
                return 1;
            
            var (overlapStart, overlapEnd) = GetOverlapStartAndEnd(ts1, ts2);
            return GetTimestampDifference(overlapStart, overlapEnd);
        }

        private static (Timestamp, Timestamp) GetOverlapStartAndEnd(MoonVisibleInterval ts1, MoonVisibleInterval ts2)
        {
            var overlapStart = ts2.RiseTime.Absolute > ts1.RiseTime.Absolute ? ts2.RiseTime : ts1.RiseTime;
            var overlapEnd = ts2.SetTime.Absolute < ts1.SetTime.Absolute ? ts2.SetTime : ts1.SetTime;
            return (overlapStart, overlapEnd);
        }

        private static int GetTimestampDifference(Timestamp start, Timestamp end)
        {
            var overlap = end.Absolute - start.Absolute;
            return overlap < 0 ? 0 : overlap;
        }
    }
}