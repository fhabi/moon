﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Moon
{
    public class Program
    {
        private static readonly string MoonAscii = File.ReadAllText("moon.txt");
        
        private static void Main(string[] args)
        {
            char key;
            do
            {
                Console.Clear();
                PrintIntroduction();
                Console.Write("Enter rise and set times: ");
                var input = Console.ReadLine();
                
                var overlap = GetMarsMoonIntervalsOverlap(input);
                Console.WriteLine(overlap == null ? "ERROR: Invalid format." : $"Overlapping minutes: {overlap}");
                
                Console.WriteLine("Enter (q) to quit or any other key to continue.");
                key = Console.ReadKey().KeyChar;
            } while (!key.Equals('q'));
        }

        private static void PrintIntroduction()
        {
            Console.WriteLine(MoonAscii);
            Console.WriteLine("Enter the Mars moons' rise and set times in the following format:");
            Console.WriteLine("D = Deimos, P = Phobos, RT = Rise Time (hh:mm), ST = Set Time (hh:mm)");
            Console.WriteLine("D[RT, ST] P[RT, ST]");
            Console.WriteLine("Example: D[18:55, 04:97] P[10:39, 04:00]");
            Console.WriteLine("══════════════════════════════════════════════════════════════════════════════════\n");

        }

        public static int? GetMarsMoonIntervalsOverlap(string input)
        {
            var inputRegex = new Regex(@"^(?<deimos>D(\[\d{1,2}:\d{1,2},\s*\d{1,2}:\d{1,2}\]))\s*(?<phobos>P(\[\d{1,2}:\d{1,2},\s*\d{1,2}:\d{1,2}\]))$", RegexOptions.Compiled);
            var matches = inputRegex.Match(input);

            if (!matches.Success)
                return null;
            
            var deimos = new MoonVisibleInterval(matches.Groups["deimos"].Value);
            var phobos = new MoonVisibleInterval(matches.Groups["phobos"].Value);

            return deimos.GetIntervalOverlapWith(phobos);
        }
    }
}
