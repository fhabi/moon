using System.Text.RegularExpressions;

namespace Moon
{
    public static class Constants
    {
        public static readonly Regex TimestampRegex = new(@"(?<h>\d{1,2}):(?<m>\d{1,2})");
        public static readonly Regex MoonVisibleIntervalRegex = new(@"\w{1}(\[(?<start>\d{1,2}:\d{1,2}),\s*(?<end>\d{1,2}:\d{1,2})\])");
    }
}