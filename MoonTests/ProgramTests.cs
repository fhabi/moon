using NUnit.Framework;
using Moon;

namespace MoonTests
{
    public class Tests
    {
        [TestCase("D[18:55, 21:47] P[10:39, 20:87]", TestName = "TC01", Category = "AK01", ExpectedResult = 232)]
        [TestCase("D[18:55, 21:47] P[18:54, 21:46]", TestName = "TC02", Category = "AK01", ExpectedResult = 291)]
        [TestCase("D[18:55, 21:47] P[19:67, 22:95]", TestName = "TC03", Category = "AK02", ExpectedResult = 180)]
        [TestCase("D[18:55, 21:47] P[18:56, 21:48]", TestName = "TC04", Category = "AK02", ExpectedResult = 291)]
        [TestCase("D[18:55, 21:47] P[19:67, 20:95]", TestName = "TC05", Category = "AK03", ExpectedResult = 128)]
        [TestCase("D[18:55, 21:47] P[18:56, 21:46]", TestName = "TC06", Category = "AK03", ExpectedResult = 290)]
        [TestCase("D[18:55, 21:47] P[15:67, 24:95]", TestName = "TC07", Category = "AK04", ExpectedResult = 292)]
        [TestCase("D[18:55, 21:47] P[18:54, 21:48]", TestName = "TC08", Category = "AK04", ExpectedResult = 292)]
        [TestCase("D[18:55, 21:47] P[18:11, 18:55]", TestName = "TC09", Category = "AK05", ExpectedResult = 1)]
        [TestCase("D[18:55, 21:47] P[21:47, 23:21]", TestName = "TC10", Category = "AK06", ExpectedResult = 1)]
        [TestCase("D[18:55, 04:97] P[10:39, 04:00]", TestName = "TC12", Category = "AK07", ExpectedResult = 1045)]
        [TestCase("D[18:55, 04:97] P[01:00, 04:00]", TestName = "TC13", Category = "AK08", ExpectedResult = 300)]
        [TestCase("D[18:55, 20:11] P[12:32, 16:21]", TestName = "TC14", Category = "AK09", ExpectedResult = 0)]
        [TestCase("D[18:a5, 20:11]; P[12:2, 16:21]", TestName = "TC15", Category = "AK10", ExpectedResult = null)]
        [TestCase("D[18:00, 04:00] P[03:00, 19:00]", TestName = "TC16", Category = "AK11", ExpectedResult = 200)]
        public int? InputOutput_Tests(string input)
        {
            return Program.GetMarsMoonIntervalsOverlap(input);
        }

        [Test]
        public void TC11_GetMarsMoonIntervalsOverlap_IsCommutative()
        {
            var overlap1 = Program.GetMarsMoonIntervalsOverlap("D[18:55, 21:47] P[10:39, 20:87]");   
            var overlap2 = Program.GetMarsMoonIntervalsOverlap("D[10:39, 20:87] P[18:55, 21:47]");
            Assert.AreEqual(overlap1, overlap2);
            
            overlap1 = Program.GetMarsMoonIntervalsOverlap("D[18:55, 21:47] P[18:56, 21:46]");   
            overlap2 = Program.GetMarsMoonIntervalsOverlap("D[18:56, 21:46] P[18:55, 21:47]");
            Assert.AreEqual(overlap1, overlap2);
            
            overlap1 = Program.GetMarsMoonIntervalsOverlap("D[18:55, 04:97] P[01:00, 04:00]");   
            overlap2 = Program.GetMarsMoonIntervalsOverlap("D[01:00, 04:00] P[18:55, 04:97]");
            Assert.AreEqual(overlap1, overlap2);
        }
    }
}